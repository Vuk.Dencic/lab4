package datastructure;

import cellular.CellState;

import java.util.ArrayList;

public class CellGrid implements IGrid {
    int rows;
    int cols;
    //this list shall store all the
    ArrayList<CellState> list;


    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        this.list = new ArrayList<CellState>();

        int numEntries = rows*columns;
        for(int i=0; i<numEntries; i++) {
            list.add(initialState);
        }

    }

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int col, CellState element) {
        int index = getIndex(row,col);
        list.set(index, element);
    }

    private int getIndex(int row, int col) {
        if(row >= this.numRows() || col>=this.numColumns()) {
            throw new IndexOutOfBoundsException("Too high index");
        }
        if(row < 0 || col<0) {
            throw new IndexOutOfBoundsException("Too low index");
        }
        return row*this.cols + col;
    }

    @Override
    public CellState get(int row, int column) {
        int index = getIndex(row,column);
        return this.list.get(index);

    }

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for(int row=0; row<this.rows; row++) {
            for(int col=0; col<this.cols;col++) {
                CellState value = this.get(row, col);
                newGrid.set(row, col, value);
            }
        }
        return newGrid;

    }
    
}
