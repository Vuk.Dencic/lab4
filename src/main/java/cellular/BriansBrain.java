package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;

    public BriansBrain(int rows, int cols) {
        currentGeneration = new CellGrid(rows, cols, CellState.DEAD);
        initializeCells();

    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int x = 0; x < currentGeneration.numRows(); x++){
            for(int y = 0; y < currentGeneration.numColumns(); y++){
                if (random.nextBoolean()){
                    currentGeneration.set(x,y, CellState.ALIVE);
                }
                else{
                    currentGeneration.set(x,y, CellState.DEAD);
                }
            }
        }

    }

    @Override
    public void step() {
        IGrid nextExplorer = new CellGrid(
                currentGeneration.numRows(), currentGeneration.numColumns(), CellState.ALIVE);

        for (int x = 0; x < currentGeneration.numRows(); x++){
            for (int y = 0; y < currentGeneration.numColumns(); y++){
                nextExplorer.set(x,y, getNextCell(x,y));
            }
        }
        currentGeneration = nextExplorer;

    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState currentState = currentGeneration.get(row, col);
        if (currentState == CellState.ALIVE) {
            return CellState.DYING;
        }
        if (currentState == CellState.DYING) {
            return CellState.DEAD;
        }
        int numNeighbours = getNumNeighbours(row, col);

        if (currentState == CellState.DEAD && numNeighbours == 2) {
            return CellState.ALIVE;
        }
        return CellState.DEAD;
        //return currentGeneration.get(row, col)

    }

    private int getNumNeighbours(int row, int col) {
        int numNeighbours = 0;
        for (int dx = -1; dx <= 1; dx++){
            for (int dy = -1; dy <= 1; dy++){
                if(dx == 0 && dy == 0){
                    continue;
                }
                if(col + dy < 0){
                    continue;
                }
                if(col + dy >= currentGeneration.numColumns()){
                    continue;
                }
                if(row + dx < 0){
                    continue;
                }
                if(row + dx >= currentGeneration.numRows()){
                    continue;
                }
                if(currentGeneration.get(row + dx, col + dy) == CellState.ALIVE){
                    numNeighbours++;
                }

            }
        }
        return numNeighbours;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
